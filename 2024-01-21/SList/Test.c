#include"SList.h"

//void SlistTest01() {
//	//一般不会这样去创建链表，这里只是为了给大家展示链表的打印
//	SLTNode* node1 = (SLTNode*)malloc(sizeof(SLTNode));
//	node1->data = 1;
//	SLTNode* node2 = (SLTNode*)malloc(sizeof(SLTNode));
//	node2->data = 2;
//	SLTNode* node3 = (SLTNode*)malloc(sizeof(SLTNode));
//	node3->data = 3;
//	SLTNode* node4 = (SLTNode*)malloc(sizeof(SLTNode));
//	node4->data = 4;
//
//	node1->next = node2;
//	node2->next = node3;
//	node3->next = node4;
//	node4->next = NULL;
//
//	SLTNode* plist = node1;
//	SLTPrint(plist);
//}
void SlistTest02() {
	SLTNode* plist = NULL;
	SLTPushBack(&plist, 1);
	SLTPushBack(&plist, 2);
	SLTPushBack(&plist, 3);
	SLTPushBack(&plist, 4);
	SLTPrint(plist); //1->2->3->4->NULL

	//SLTPushFront(&plist, 5);
	//SLTPrint(plist);          //5->1->2->3->4->NULL
	//SLTPushFront(&plist, 6);
	//SLTPrint(plist);         //6->5->1->2->3->4->NULL
	//SLTPushFront(&plist, 7);
	//SLTPrint(plist);         //7-6->5->1->2->3->4->NULL

	SLTPopBack(&plist);
	SLTPrint(plist);//1->2->3->NULL
	SLTPopBack(&plist);
	SLTPrint(plist);//1->2->3->NULL
	SLTPopBack(&plist);
	SLTPrint(plist);//1->2->3->NULL
	SLTPopBack(&plist);
	SLTPrint(plist);//1->2->3->NULL
	SLTPopBack(&plist);
	SLTPrint(plist);//1->2->3->NULL
}

void SlistTest03() {
	SLTNode* plist = NULL;
	SLTPushBack(&plist, 1);
	SLTPushBack(&plist, 2);
	SLTPushBack(&plist, 3);
	SLTPushBack(&plist, 4);
	SLTPrint(plist); //1->2->3->4->NULL

	SListDesTroy(&plist);
	////头删
	//SLTPopFront(&plist);
	//SLTPrint(plist);    //2->3->4->NULL
	//SLTPopFront(&plist);
	//SLTPrint(plist);    //3->4->NULL
	//SLTPopFront(&plist);
	//SLTPrint(plist);    //4->NULL
	//SLTPopFront(&plist);
	//SLTPrint(plist);    //NULL
	//SLTPopFront(&plist);
	//SLTPrint(plist);    //assert
	//
	//SLTNode* FindRet = SLTFind(&plist, 3);
	//if (FindRet) {
	//	printf("找到了！\n");
	//}
	//else {
	//	printf("未找到！\n");
	//}
	//SLTInsert(&plist, FindRet, 100);
	//SLTInsertAfter(FindRet, 100);
	//
	//删除指定位置的节点
	//SLTErase(&plist, FindRet);
	//SLTPrint(plist); //1->2->3->NULL
}
int main() {
	//SlistTest01();
	//SlistTest02();
	SlistTest03();
	return 0;
}